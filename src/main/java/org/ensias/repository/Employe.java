package org.ensias.repository;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Employe {
	
	@Id
	@GeneratedValue
	private int id;
	private String nom;
	private String cours;
	
	
}
