package org.ensias.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(collectionResourceRel="employes", path="employes")
public interface EmployeRepo extends JpaRepository<Employe, Integer>{
	

}
