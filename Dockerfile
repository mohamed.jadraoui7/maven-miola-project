FROM maven:3.8.1-adoptopenjdk-8


ADD /target/cours-springboot-rest-0.0.1-SNAPSHOT.jar cours-springboot-rest-0.0.1-SNAPSHOT.jar

CMD ["java","-jar","cours-springboot-rest-0.0.1-SNAPSHOT.jar"]
